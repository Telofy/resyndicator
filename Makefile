SHELL = /bin/bash
PYTHON = $(shell which python3.7 || which python3.6)

default: install

bin/pip:
	${PYTHON} -m venv .

install: bin/pip
	bin/pip install --upgrade pip setuptools
	bin/pip install .

clean:
	rm -Rf bin include lib local
